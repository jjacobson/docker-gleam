FROM golang:1.8-alpine3.6

RUN set -ex && \
    apk --update add git && \
    go get -v github.com/chrislusf/gleam/flow && \
    go get -v github.com/chrislusf/gleam/distributed/gleam && \
    apk del git

COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

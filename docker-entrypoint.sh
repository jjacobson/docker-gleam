#!/bin/sh

set -e

if [ $1 == "agent" ]; then
    if [ -z "$MASTER" ]; then
        echo "Master is empty"
        exit 1
    fi

    exec gleam agent \
        --master=${MASTER}:45326 \
        --memory=${MEMORY:-1024} \
        --executor.max=${MAX_EXECUTORS:-8} \
        --executor.cpu.level=${EXECUTOR_CPU_LEVEL:-1}
elif [ $1 == "master" ]; then
    exec gleam master
else
    exec "$@"
fi
